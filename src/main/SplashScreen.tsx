import BaseComponent from "../component/BaseComponent";
import BaseScreen from "./BaseScreen";
import { View, Text } from "native-base";
import { Colors } from "react-native/Libraries/NewAppScreen";
import React from "react";
import { StackActions } from "@react-navigation/native";
class SplashScreenProperties{
    navigation:any;
}

class SplashScreenState{

}

export default class SplashScreen extends BaseScreen<SplashScreenProperties,SplashScreenState>{
    constructor(props:SplashScreenProperties){
        super(props);
    }

    render():any{
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:Colors.green }}>
              <Text>TS APP</Text>
            </View>
          );
    }

    componentDidMount(){
        setTimeout(()=>{
            this.navigator.navigate("Profile");
        },3000)
    }

}