import PropTypes from 'prop-types';
import string from 'prop-types';
export default interface ButtonProperties {
     color:string;
     fontSize:string;
     fontWidth:string;
}