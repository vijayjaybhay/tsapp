import BaseComponent from "../../BaseComponent";
import ButtonProperties from "./ButtonProperties";
import ButtonState from "./ButtonState";
import {StyleSheet, Text} from "react-native"
import React from "react"

export default class Button extends BaseComponent<ButtonProperties,ButtonState>{
    constructor(props:ButtonProperties){
        super(props);
    }

    public renderAndroid():React.ReactElement<any>{
        let style=StyleSheet.create(  {
            tsaButton:{
              color: this.props.color,
               fontWeight: 'normal',
               textAlign:"center",
               fontSize: 89, 
            }
        });
        return(  <Text style={style.tsaButton}>OK</Text>);
    }

    public renderIos():any{
        let style=StyleSheet.create(  {
            tsaButton:{
              color: this.props.color,
               fontWeight: 'normal',
               textAlign:"center",
               fontSize: 89, 
            }
        });
        return(  <Text style={style.tsaButton}>OK</Text>);
    }
}