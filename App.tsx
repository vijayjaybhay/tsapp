import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { ReactNode } from 'react'
import React from 'react'
import {Content, Icon, Input} from "native-base"
import {Item} from "native-base"
import SplashScreen from './src/main/SplashScreen';
export const Stack = createStackNavigator();
const App: () => ReactNode = () => {

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer >
          <Stack.Navigator>
            <Stack.Screen
              name="Splash Screen"
              component={SplashScreen}
              options={{title: 'Welcome',headerShown:false}}/>
            <Stack.Screen name="Profile" component={DetailsScreen} />
          </Stack.Navigator>
        </NavigationContainer>
    </>
  );
};

function HomeScreen({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:Colors.green }}>
      <Text>Home Screen</Text>
      <Button
      title="Go to Details"
      onPress={() => navigation.navigate('Profile', {name: 'Jane'})} />
    </View>
  );
}

function DetailsScreen({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
    flex:1,
    flexDirection:"row",  
  },
  contentStyle:{
    backgroundColor: Colors.white,
    flex:1,
    flexDirection:"row",  
    alignItems:"center"
  },
  item:{
    paddingStart:25,
    paddingEnd:25,
  
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
